package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

type User struct {
	Nick          string       `json:"Nick"`
	Email         string       `json:"Email"`
	CreatedAt     string       `json:"created_at"`
	Subscribers   []Subscriber `json:"Subscribers"`
	Subscriptions []Subscriber
}

type Subscriber struct {
	Email     string `json:"email"`
	CreatedAt string `json:"created_at"`
}

type UserPair struct {
	id   int
	from string
	to   string
}

type ResultStruct struct {
	ID   int          `json:"id"`
	From string       `json:"from"`
	To   string       `json:"to"`
	Path []Subscriber `json:"path,omitempty"`
}

const (
	usersFileName     = "users.json"
	usersPairFileName = "input.csv"
	resultFileName    = "result.json"
)

func main() {
	users, err := readUsersFromFile(usersFileName)
	if err != nil {
		log.Fatal(err)
	}

	usersMap := make(map[string]User)
	for _, user := range users {
		usersMap[user.Email] = user
	}

	usersWithSubscriptions := calculateSubscriptions(usersMap)

	userPairs, err := readUserPairFromFile(usersPairFileName)
	if err != nil {
		log.Fatal(err)
	}

	var result []ResultStruct
	for _, pair := range userPairs {
		result = append(result, calculateResultStruct(pair, usersWithSubscriptions))
	}

	err = writeJSONFile(resultFileName, result)
	if err != nil {
		log.Fatal(err)
	}
}

func calculateResultStruct(pair UserPair, users map[string]User) ResultStruct {
	path := bfs(users[pair.from], users, pair.to)

	if len(path) != 0 {
		path = path[1 : len(path)-1]
	}

	var resultPath []Subscriber
	for _, u := range path {
		resultPath = append(resultPath, Subscriber{u.Email, u.CreatedAt})
	}

	return ResultStruct{pair.id, pair.from, pair.to, resultPath}
}

func bfs(user User, users map[string]User, requiredUserEmail string) []User {
	if user.Email == requiredUserEmail {
		return nil
	}

	explored := make(map[string]User)

	queue := [][]User{{user}}

	for len(queue) > 0 {
		path := queue[0]
		queue = queue[1:]

		node := path[len(path)-1]
		if _, ok := explored[node.Email]; !ok {
			explored[node.Email] = node

			for _, sub := range node.Subscriptions {
				newPath := append(path, users[sub.Email])
				queue = append(queue, newPath)

				if sub.Email == requiredUserEmail {
					return newPath
				}
			}
		}
	}

	return nil
}

func calculateSubscriptions(usersMap map[string]User) map[string]User {
	for _, user := range usersMap {
		for _, subscriber := range user.Subscribers {
			subscriptions := append(usersMap[subscriber.Email].Subscriptions, Subscriber{user.Email, user.CreatedAt})
			u := usersMap[subscriber.Email]
			usersMap[subscriber.Email] = User{u.Nick, u.Email, u.CreatedAt, u.Subscribers, subscriptions}
		}
	}

	return usersMap
}

func readUsersFromFile(fileName string) ([]User, error) {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, fmt.Errorf("can't read from file %s: %s", fileName, err)
	}

	var users []User

	err = json.Unmarshal(file, &users)
	if err != nil {
		return nil, fmt.Errorf("can't unmarshal json from file %s: %s", fileName, err)
	}

	return users, nil
}

func readUserPairFromFile(fileName string) ([]UserPair, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, fmt.Errorf("can't open file %s: %s", fileName, err)
	}
	defer file.Close()

	reader := csv.NewReader(file)

	data, err := reader.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("can't read from file %s: %s", fileName, err)
	}

	var pairs []UserPair
	for i, pair := range data {
		pairs = append(pairs, UserPair{i + 1, pair[0], pair[1]})
	}

	return pairs, nil
}

func writeJSONFile(fileName string, value []ResultStruct) error {
	file, err := os.Create(fileName)
	if err != nil {
		return fmt.Errorf("can't create file %s: %s", fileName, err)
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	encoder.SetIndent("  ", "    ")

	err = encoder.Encode(value)
	if err != nil {
		return fmt.Errorf("can't encode value into file %s: %s", fileName, err)
	}

	return nil
}
